﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheModernMilkman.TechTest.Data.Models
{
    public class Customer
    {
        [Required, StringLength(20)]
        public string Title { get; set; }

        [Required, StringLength(50)]
        public string Forename { get; set; }

        [Required, StringLength(50)]
        public string Surname { get; set; }

        [Key, Required, StringLength(75)]
        public string EmailAddress { get; set; }

        [Required, StringLength(15)]
        public string MobileNo { get; set; }

        public CustomerStatus Status { get; set; }

        public List<Address> Addresses { get; set; }

        #region Nested Types

        [Flags]
        public enum CustomerStatus
        {
            Active,
            Inactive
        }

        #endregion Nested Types
    }
}
