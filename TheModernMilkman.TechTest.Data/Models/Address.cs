﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TheModernMilkman.TechTest.Data.Models
{
    public class Address
    {
        #region Default Values

        private string defaultCountry = "UK";

        #endregion Default Values

        [Key]
        public Guid AddressId { get; set; }

        [Required, StringLength(80)]
        public string AddressLine1 { get; set; }

        [StringLength(80)]
        public string AddressLine2 { get; set; }

        [Required, StringLength(50)]
        public string Town { get; set; }

        [MaxLength(50)]
        public string County { get; set; }

        [Required, StringLength(10)]
        public string Postcode { get; set; }

        [DefaultValue("UK")]
        public string Country
        {
            get
            {
                return defaultCountry;
            }
            set
            {
                defaultCountry = value;
            }
        }

        public bool IsMainAddress { get; set; }
    }
}
