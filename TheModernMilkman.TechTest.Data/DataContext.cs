﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using TheModernMilkman.TechTest.Data.Models;

namespace TheModernMilkman.TechTest.Data
{
    public class DataContext
    {
        private readonly string jsonDataPath;

        public List<Customer> customers;

        public DataContext(IConfiguration configuration)
        {
            jsonDataPath = Path.Combine(AppContext.BaseDirectory, configuration.GetConnectionString("Database"));
            ReadJsonData().Wait();
        }

        private async Task ReadJsonData()
        {
            var jsonData = JObject.Parse(await File.ReadAllTextAsync(jsonDataPath));
            customers = jsonData.SelectToken(nameof(customers)).ToObject<List<Customer>>();
        }

        public async Task SaveJsonData()
        {
            var jsonData = JsonConvert.SerializeObject(new
            {
                customers = customers
            });

            await File.WriteAllTextAsync(jsonDataPath, jsonData);
        }
    }
}
