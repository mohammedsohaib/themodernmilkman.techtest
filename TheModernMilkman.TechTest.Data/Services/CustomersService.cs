﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TheModernMilkman.TechTest.Data.Models;

namespace TheModernMilkman.TechTest.Data.Services
{
    public class CustomersService
    {
        private readonly DataContext dataContext;

        public CustomersService(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<Customer> Create(Customer customer)
        {
            dataContext.customers.Add(
                new Customer
                {
                    Title = customer.Title,
                    Forename = customer.Forename,
                    Surname = customer.Surname,
                    EmailAddress = customer.EmailAddress,
                    MobileNo = customer.MobileNo
                });

            await dataContext.SaveJsonData();

            return await Get(customer.EmailAddress);
        }

        public async Task<List<Customer>> List()
        {
            return dataContext.customers;
        }

        public async Task<Customer> Get(string emailAddress)
        {
            return dataContext.customers.SingleOrDefault(c => c.EmailAddress.ToLower() == emailAddress.ToLower());
        }

        public async Task<Customer> Update(string emailAddress, Customer customerUpdate)
        {
            var customer = await Get(emailAddress);

            customer.Title = customerUpdate.Title;
            customer.Forename = customerUpdate.Forename;
            customer.Surname = customerUpdate.Surname;
            customer.EmailAddress = customerUpdate.EmailAddress;
            customer.MobileNo = customerUpdate.MobileNo;

            await dataContext.SaveJsonData();

            return await Get(customerUpdate.EmailAddress);
        }

        public async Task<Customer> UpdateStatus(string emailAddress, Customer.CustomerStatus customerStatus)
        {
            var customer = await Get(emailAddress);

            customer.Status = customerStatus;

            await dataContext.SaveJsonData();

            return await Get(customer.EmailAddress);
        }

        public async Task Delete(string emailAddress)
        {
            var customer = await Get(emailAddress);

            dataContext.customers.Remove(customer);

            await dataContext.SaveJsonData();
        }

        public async Task<Customer> AddAddress(string emailAddress, Address address)
        {
            var customer = await Get(emailAddress);

            address.AddressId = Guid.NewGuid();

            if (customer.Addresses == null)
                customer.Addresses = new List<Address>();

            customer.Addresses.Add(address);

            await dataContext.SaveJsonData();

            return await Get(emailAddress);
        }

        public async Task<Customer> UpdateAddress(string emailAddress, Guid addressId, Address addressUpdate)
        {
            var customer = await Get(emailAddress);

            var address = customer.Addresses.Single(a => a.AddressId == addressId);

            address.AddressLine1 = addressUpdate.AddressLine1;
            address.AddressLine2 = addressUpdate.AddressLine2;
            address.Town = addressUpdate.Town;
            address.County = addressUpdate.County;
            address.Postcode = addressUpdate.Postcode;
            address.Country = addressUpdate.Country;

            await dataContext.SaveJsonData();

            return await Get(emailAddress);
        }

        public async Task<Customer> ChangeMainAddress(string emailAddress, Guid addressId)
        {
            var customer = await Get(emailAddress);

            var currentMainAddress = customer.Addresses.Single(a => a.IsMainAddress);
            var address = customer.Addresses.Single(a => a.AddressId == addressId);

            if (currentMainAddress != null && currentMainAddress.AddressId != addressId)
            {
                currentMainAddress.IsMainAddress = false;
            }

            address.IsMainAddress = true;

            await dataContext.SaveJsonData();

            return await Get(emailAddress);
        }

        public async Task DeleteAddress(string emailAddress, Guid addressId)
        {
            var customer = await Get(emailAddress);

            var address = customer.Addresses.Single(a => a.AddressId == addressId);

            if (customer.Addresses.Count() > 1)
            {
                customer.Addresses.Remove(address);
            }

            await dataContext.SaveJsonData();
        }
    }
}
