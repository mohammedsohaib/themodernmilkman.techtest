﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using TheModernMilkman.TechTest.Data.Models;
using TheModernMilkman.TechTest.Data.Services;

namespace TheModernMilkman.TechTest.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {     
        private readonly ILogger<CustomersController> logger;
        private readonly CustomersService customersService;

        public CustomersController(ILogger<CustomersController> logger, CustomersService customersService)
        {
            this.logger = logger;
            this.customersService = customersService;
        }

        #region Customer Management

        /// <summary>
        /// Creates the specified customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create(Customer customer)
        {
            if (customer == null)
            {
                return BadRequest("Customer details must be supplied for it to be created");
            }

            if (await customersService.Get(customer.EmailAddress) != null)
            {
                return BadRequest($"A customer with the email address {customer.EmailAddress} already exists");
            }

            customer = await customersService.Create(customer);

            logger.LogInformation($"Customer {customer.EmailAddress} added successfully");

            return Ok(customer);
        }

        /// <summary>
        /// Lists the specified show active customers.
        /// </summary>
        /// <param name="showActiveCustomers">if set to <c>true</c> [show active customers].</param>
        /// <returns></returns>
        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> List(bool showActiveCustomers)
        {
            var customers = await customersService.List();

            if (showActiveCustomers)
            {
                customers = customers.Where(c => c.Status == Customer.CustomerStatus.Active).ToList();
            }

            return Ok(customers);
        }

        /// <summary>
        /// Gets the customer specified by their email address.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> Get(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return BadRequest("An email address must be provided in order to fetch the customer");
            }

            var customer = await customersService.Get(emailAddress);

            if (customer == null)
            {
                return BadRequest($"A customer cannot be found with the email address {emailAddress}");
            }

            return Ok(customer);
        }

        /// <summary>
        /// Updates the customer specified by their email address.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="customerUpdate">The customer update.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(string emailAddress, Customer customerUpdate)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return BadRequest("An email address must be provided in order to fetch and update the customer");
            }

            if (customerUpdate == null)
            {
                return BadRequest("Customer details must be supplied for it to be updated");
            }

            var customer = await customersService.Get(emailAddress);

            if (customer == null)
            {
                return BadRequest($"A customer cannot be found with the email address {emailAddress}");
            }

            customer = await customersService.Update(emailAddress, customerUpdate);

            return Ok(customer);
        }

        /// <summary>
        /// Updates the customer status.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="customerStatus">The customer status.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update/Status")]
        public async Task<IActionResult> UpdateStatus(string emailAddress, Customer.CustomerStatus customerStatus)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return BadRequest("An email address must be provided in order to fetch and update the customer");
            }

            var customer = await customersService.Get(emailAddress);

            if (customer == null)
            {
                return BadRequest($"A customer cannot be found with the email address {emailAddress}");
            }

            customer = await customersService.UpdateStatus(emailAddress, customerStatus);

            return Ok(customer);
        }

        /// <summary>
        /// Deletes the customer specified by their email address.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> Delete(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return BadRequest("An email address must be provided in order to fetch and update the customer");
            }

            var customer = await customersService.Get(emailAddress);

            if (customer == null)
            {
                return BadRequest($"A customer cannot be found with the email address {emailAddress}");
            }

            await customersService.Delete(emailAddress);

            return Ok($"Successfully deleted customer with the email address {emailAddress}");
        }

        #endregion Customer Management

        #region Customer Address Management

        /// <summary>
        /// Adds the address to the customer specified by their email address.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Address/Add")]
        public async Task<IActionResult> AddAddress (string emailAddress, Address address)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return BadRequest("An email address must be provided in order to fetch the customer");
            }

            var customer = await customersService.Get(emailAddress);

            if (customer == null)
            {
                return BadRequest($"A customer cannot be found with the email address {emailAddress}");
            }

            customer = await customersService.AddAddress(emailAddress, address);

            return Ok(customer);
        }

        /// <summary>
        /// Updates the address of the customer specified by their email address and address identifier
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="addressId">The address identifier.</param>
        /// <param name="addressUpdate">The address update.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Address/Update")]
        public async Task<IActionResult> UpdateAddress(string emailAddress, Guid addressId, Address addressUpdate)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return BadRequest("An email address must be provided in order to fetch and update the customer");
            }

            if (addressUpdate == null)
            {
                return BadRequest("Address details must be supplied for it to be updated");
            }

            var customer = await customersService.Get(emailAddress);

            if (customer == null)
            {
                return BadRequest($"A customer cannot be found with the email address {emailAddress}");
            }


            if (!customer.Addresses.Any(a => a.AddressId == addressId))
            {
                return BadRequest($"This address does not exist against this customer record");
            }

            customer = await customersService.UpdateAddress(emailAddress, addressId, addressUpdate);

            return Ok(customer);
        }

        /// <summary>
        /// Changes the main address of the customer specified by their email address and address identifier
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="addressId">The address identifier.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Address/ChangeMainAddress")]
        public async Task<IActionResult> ChangeMainAddress(string emailAddress, Guid addressId)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return BadRequest("An email address must be provided in order to fetch the customer");
            }

            var customer = await customersService.Get(emailAddress);

            if (customer == null)
            {
                return BadRequest($"A customer cannot be found with the email address {emailAddress}");
            }

            if (!customer.Addresses.Any(a => a.AddressId == addressId))
            {
                return BadRequest($"This address does not exist against this customer record");
            }

            customer = await customersService.ChangeMainAddress(emailAddress, addressId);

            return Ok(customer);
        }

        /// <summary>
        /// Deletes the address of the customer specified by their email address and address identifier
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="addressId">The address identifier.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Address/Delete")]
        public async Task<IActionResult> DeleteAddress(string emailAddress, Guid addressId)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return BadRequest("An email address must be provided in order to fetch the customer");
            }

            var customer = await customersService.Get(emailAddress);

            if (customer == null)
            {
                return BadRequest($"A customer cannot be found with the email address {emailAddress}");
            }

            if (!customer.Addresses.Any(a => a.AddressId == addressId))
            {
                return BadRequest($"This address does not exist against this customer record");
            }

            await customersService.DeleteAddress(emailAddress, addressId);

            return Ok($"Successfully deleted address for customer with the email address {emailAddress}");
        }

        #endregion Customer Address Management
    }
}