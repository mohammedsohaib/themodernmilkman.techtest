using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TheModernMilkman.TechTest.Api.Controllers;
using TheModernMilkman.TechTest.Data.Models;
using TheModernMilkman.TechTest.Data.Services;

namespace TheModernMilkman.TechTest.Tests.Controllers
{
    [TestClass]
    public class CustomersControllerTests : BaseTestSetup
    {
        private readonly CustomersController customersController;

        public CustomersControllerTests()
        {
            customersController = new CustomersController(GetMockLogger<CustomersController>(), (CustomersService)serviceProvider.GetService(typeof(CustomersService)));
            customersController.ControllerContext = new ControllerContext() { HttpContext = new DefaultHttpContext() };
        }

        [TestMethod]
        public async Task Test_CreateCustomer()
        {
            var customer = new Customer
            {
                Title = "Mr",
                Forename = "Unit",
                Surname = "Tester",
                EmailAddress = "unittester@tester.co.uk",
                MobileNo = "07376289812"
            };

            ObjectResult result = (await customersController.Create(customer)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOfType(result.Value, typeof(Customer));

            var resultData = (Customer)result.Value;
            Assert.AreEqual(resultData.Title, customer.Title);
            Assert.AreEqual(resultData.Forename, customer.Forename);
            Assert.AreEqual(resultData.Surname, customer.Surname);
            Assert.AreEqual(resultData.EmailAddress, customer.EmailAddress);
            Assert.AreEqual(resultData.Title, customer.Title);
        }

        [TestMethod]
        public async Task Test_GetListOfActiveCustomers()
        {
            ObjectResult result = (await customersController.List(true)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOfType(result.Value, typeof(List<Customer>));

            var resultData = (List<Customer>)result.Value;
            Assert.IsFalse(resultData.Any(c => c.Status == Customer.CustomerStatus.Inactive));
            Assert.IsTrue(resultData.Any(c => c.Status == Customer.CustomerStatus.Active));
        }

        [TestMethod]
        public async Task Test_GetListOfCustomers()
        {
            ObjectResult result = (await customersController.List(false)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOfType(result.Value, typeof(List<Customer>));

            var resultData = (List<Customer>)result.Value;
            Assert.IsTrue(resultData.Any(c => c.Status == Customer.CustomerStatus.Inactive));
            Assert.IsTrue(resultData.Any(c => c.Status == Customer.CustomerStatus.Active));
        }

        [TestMethod]
        public async Task Test_GetCustomerByEmail()
        {
            var result = (await customersController.Get("joebloggs@tester.co.uk")) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOfType(result.Value, typeof(Customer));
        }

        [TestMethod]
        public async Task Test_UpdateCustomer()
        {
            var customer = new Customer
            {
                Title = "Mr",
                Forename = "Unit",
                Surname = "Tester",
                EmailAddress = "unitteste74747@tester.co.uk",
                MobileNo = "07376289812"
            };

            ObjectResult result = (await customersController.Create(customer)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);

            var updatedCustomer = new Customer
            {
                Title = "Mr",
                Forename = "Joe",
                Surname = "Testing",
                EmailAddress = "unittesting@tester.co.uk",
                MobileNo = "07376289852"
            };

            result = (await customersController.Update(customer.EmailAddress, updatedCustomer)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOfType(result.Value, typeof(Customer));

            var resultData = (Customer)result.Value;
            Assert.AreEqual(resultData.Title, updatedCustomer.Title);
            Assert.AreEqual(resultData.Forename, updatedCustomer.Forename);
            Assert.AreEqual(resultData.Surname, updatedCustomer.Surname);
            Assert.AreEqual(resultData.EmailAddress, updatedCustomer.EmailAddress);
            Assert.AreEqual(resultData.Title, updatedCustomer.Title);
        }

        [TestMethod]
        public async Task Test_UpdateStatus()
        {
            ObjectResult result = (await customersController.UpdateStatus("joebloggs@tester.co.uk", Customer.CustomerStatus.Active)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOfType(result.Value, typeof(Customer));

            var resultData = (Customer)result.Value;
            Assert.AreEqual(resultData.Status, Customer.CustomerStatus.Active);

            result = (await customersController.UpdateStatus("joebloggs@tester.co.uk", Customer.CustomerStatus.Inactive)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOfType(result.Value, typeof(Customer));

            resultData = (Customer)result.Value;
            Assert.AreEqual(resultData.Status, Customer.CustomerStatus.Inactive);
        }

        [TestMethod]
        public async Task Test_DeleteCustomer()
        {
            var customer = new Customer
            {
                Title = "Mr",
                Forename = "Unit",
                Surname = "Tester",
                EmailAddress = "duigbsuet4y9@tester.co.uk",
                MobileNo = "07376289812"
            };

            ObjectResult result = (await customersController.Create(customer)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);

            result = (await customersController.Delete(customer.EmailAddress)) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);

            result = (await customersController.Get(customer.EmailAddress)) as BadRequestObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(400, result.StatusCode);
        }
    }
}
