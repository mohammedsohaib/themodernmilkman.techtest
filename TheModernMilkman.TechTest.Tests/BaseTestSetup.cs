﻿using System;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Moq;

using TheModernMilkman.TechTest.Data;
using TheModernMilkman.TechTest.Data.Services;

namespace TheModernMilkman.TechTest.Tests
{
    public class BaseTestSetup
    {
        public readonly IConfiguration configuration;
        public readonly ServiceProvider serviceProvider;

        public BaseTestSetup()
        {
            this.configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            var serviceCollection = new ServiceCollection();

            serviceCollection.AddSingleton(configuration);
            serviceCollection.AddSingleton<DataContext>();
            serviceCollection.AddSingleton<CustomersService>();

            this.serviceProvider = serviceCollection.BuildServiceProvider();
        }

        public ILogger<T> GetMockLogger<T>()
        {
            return new Mock<ILogger<T>>().Object;
        }
    }
}
